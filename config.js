module.exports = {
    db: {
        url: 'mongodb://localhost/messenger',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        },
    },
    facebook: {
        appId: '529915631366097',
        appSecret: '0dce3d98e830b7bd3b656b2806c64b72',
    },
    google: {
        clientId: '674367086321-c4ck07a773mfi2r6krkj396q1m0vunj7.apps.googleusercontent.com'
    }
};