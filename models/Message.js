const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    message: String,
    datetime: {
        type: String,
        default: new Date().toISOString()
    },
    user: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

const Message = mongoose.model('Message', MessageSchema);
module.exports = Message;