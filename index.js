const express = require('express');
const cors = require('cors');
const {nanoid} = require("nanoid");
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');

const users = require('./app/users');

const app = express();
app.use(express.json());
app.use(cors());
require('express-ws')(app);

const port = 8000;

app.use('/users', users);

const User = require('./models/User');
const Message = require('./models/Message');

const activeConnections = {};

app.ws('/chat', async (ws, req) => {
    const id = nanoid();
    console.log('Client connected!, id=', id);
    activeConnections[id] = ws;

    const token = req.query.token;
    const user = await User.findOne({token});

    if (!user) {
        ws.on('close', () => {
            console.log('Client disconnected, id=', id);
            delete activeConnections[id];
        });
    }

    ws.on('message', async msg => {
        const decoded = JSON.parse(msg);
        ws.send(msg);

        if (decoded.type === 'CREATE_MESSAGE') {
            const messageData = {
                message: decoded.message,
                user: decoded.user._id
            }

            const message = new Message(messageData);
            await message.save();
        }

        let messages = await Message.find().populate('user');

        if (messages.length > 30) {
            messages = messages.slice(-30)
        }

        Object.keys(activeConnections).forEach(key => {
            const connection = activeConnections[key];

            connection.send(JSON.stringify({
                type: 'NEW_MESSAGES',
                messages: messages
            }))
        });

        let activeUsers = await User.find();

        Object.keys(activeConnections).forEach(key => {
            const connection = activeConnections[key];

            connection.send(JSON.stringify({
                type: 'ACTIVE_USERS',
                users: activeUsers
            }))
        });

        ws.on('close', () => {
            console.log('Client disconnected, id=', id);
            delete activeConnections[id];
        });
    });
});

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });
};

run().catch(console.error);